#include"Windows.h"
#include<string>

constexpr int PathMax = 1024;
constexpr char pCommand[] = "JRE16\\bin\\javaw.exe -XX:+UnlockExperimentalVMOptions -XX:+UseZGC -Dfile.encoding=utf-8 -jar ";

int WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
    char pPath[PathMax];
    memset(pPath, 0, PathMax);
    if (!GetModuleFileNameA(NULL, pPath, PathMax))
    {
        std::wstring errorText = L"启动失败，错误代码：" + std::to_wstring(GetLastError());
        MessageBox(NULL, errorText.c_str(), L"错误", MB_OK);
    }
    for (size_t i = PathMax; i > 0; --i)
    {
        if (*(pPath + i) == '\\')
        {
            auto currentFolder = std::string(pPath).substr(0, i + 1);
            auto fullPath = currentFolder + pCommand + '\"' + currentFolder + "PasswordBook_java_version.jar" + '\"';
            WinExec(fullPath.c_str(), SW_SHOW);
            break;
        }
    }
}