# PasswordBook_java_version

#### 介绍
基于javafx构建的开源密码本，使用jdk16开发

#### 软件截图

1.启动界面（无可用密码本时）
![屏幕截图](https://foruda.gitee.com/images/1663468580609322771/5f067d50_8684843.png "屏幕截图")

2.1创建密码本

![屏幕截图](https://foruda.gitee.com/images/1663468737770108180/9bb94948_8684843.png "屏幕截图")

2.1.1确认信息

![屏幕截图](https://foruda.gitee.com/images/1663469318754186834/cf31efab_8684843.png "屏幕截图")

2.1.2导入密码本

![屏幕截图](https://foruda.gitee.com/images/1663469405147278534/f4b00bc3_8684843.png "屏幕截图")

2.2导入密码本

![屏幕截图](https://foruda.gitee.com/images/1663468955422046633/e9b87a88_8684843.png "屏幕截图")

3.主界面

![屏幕截图](https://foruda.gitee.com/images/1663470001207207906/4a12592b_8684843.png "屏幕截图")
![屏幕截图](https://foruda.gitee.com/images/1663470016314352431/690b8dc6_8684843.png "屏幕截图")


4.设置界面

![屏幕截图](https://foruda.gitee.com/images/1663470052019497799/36ed9f0d_8684843.png "屏幕截图")
![屏幕截图](https://foruda.gitee.com/images/1663470070819219458/8f6036f4_8684843.png "屏幕截图")

5.关于界面

![屏幕截图](https://foruda.gitee.com/images/1663470094421717645/25569c87_8684843.png "屏幕截图")
