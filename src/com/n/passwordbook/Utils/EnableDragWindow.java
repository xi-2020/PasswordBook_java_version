package com.n.passwordbook.Utils;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.awt.*;

public class EnableDragWindow implements EventHandler<MouseEvent>
{
    private final Stage myStage;
    private double sceneX;
    private double sceneY;

    public EnableDragWindow(Stage stage, Scene node)
    {
        myStage = stage;
        node.setOnMousePressed(this);
        node.setOnMouseDragged(this);
    }

    @Override
    public void handle(MouseEvent mouseEvent)
    {
        mouseEvent.consume();
        if (mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED)
        {
            //鼠标按下的事件
            sceneX = mouseEvent.getSceneX();
            sceneY = mouseEvent.getSceneY();
        }
        else if (mouseEvent.getEventType() == MouseEvent.MOUSE_DRAGGED)
        {
            //鼠标拖动的事件
            myStage.setX(mouseEvent.getScreenX() - sceneX);
            myStage.setY(mouseEvent.getScreenY() - sceneY);
        }
    }
}
