package com.n.passwordbook.Utils;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import static com.n.passwordbook.Utils.Helper.getJARPath;

public class Settings
{
    private static Properties Settings = new Properties();

    enum SettingsItems
    {
        LastPath,
        StartTimes,
        UndoLimit,
        TopMost,
        GenerateLength,
        UseUpperChar,
        UseLowerChar,
        UseSpecialChar,
        UseNumber,
        UseAnimatedBackground,
        AnimatedBackgroundOpacity
    }

    public static String getSettingsFilePath()
    {
        return getJARPath() + File.separator + "Settings.properties";
    }

    public static void createNewSettings(File settingsFile)
    {
        resetSettings(settingsFile);
    }

    public static boolean checkIfSettingsExist()
    {
        File settingsFile = new File(getSettingsFilePath());
        if (settingsFile.exists() && settingsFile.length() != 0)
        {
            loadSettingsFile(settingsFile);
            if (StringUtils.isNotEmpty(Settings.getProperty(SettingsItems.LastPath.name())))
            {
                File passwordBookFile = new File(Settings.getProperty(SettingsItems.LastPath.name()));
                return passwordBookFile.exists();
            }
            else
            {
                return false;
            }
        }
        else
        {
            createNewSettings(settingsFile);

            return false;
        }
    }

    public static boolean setPasswordBookPath(File path)
    {
        if (path.exists())
        {
            Settings.setProperty(SettingsItems.LastPath.name(), path.getPath());
            return true;
        }

        return false;
    }

    public static String getPasswordPath()
    {
        return Settings.getProperty(SettingsItems.LastPath.name());
    }

    public static void SetStartTimes()
    {
        Settings.setProperty(SettingsItems.StartTimes.name(), String.valueOf(getStartTimes() + 1));
    }

    public static long getStartTimes()
    {
        return Long.parseLong(Settings.getProperty(SettingsItems.StartTimes.name()));
    }

    public static void SetUndoLimit(int limit)
    {
        Settings.setProperty(SettingsItems.UndoLimit.name(), String.valueOf(limit));
    }

    public static void setUndoLimitFromShow(int limit)
    {
        SetUndoLimit((limit) + 1);
    }

    public static int getUndoLimit()
    {
        return Integer.parseInt(Settings.getProperty(SettingsItems.UndoLimit.name()));
    }

    public static int getUndoLimitForShow()
    {
        return Integer.parseInt(Settings.getProperty(SettingsItems.UndoLimit.name())) - 1;
    }

    public static void setTopMost(boolean isTopMost)
    {
        Settings.setProperty(SettingsItems.TopMost.name(), String.valueOf(isTopMost));
    }

    public static boolean getTopMost()
    {
        return Boolean.parseBoolean(Settings.getProperty(SettingsItems.TopMost.name()));
    }

    public static void setGenerateLength(int length)
    {
        Settings.setProperty(SettingsItems.GenerateLength.name(), String.valueOf(length));
    }

    public static int getGenerateLength()
    {
        return Integer.parseInt(Settings.getProperty(SettingsItems.GenerateLength.name()));
    }

    public static void setUseUpperChar(boolean useUpperChar)
    {
        Settings.setProperty(SettingsItems.UseUpperChar.name(), String.valueOf(useUpperChar));
    }

    public static boolean getUseUpperChar()
    {
        return Boolean.parseBoolean(Settings.getProperty(SettingsItems.UseUpperChar.name()));
    }

    public static void setUseLowerChar(boolean useLowerChar)
    {
        Settings.setProperty(SettingsItems.UseLowerChar.name(), String.valueOf(useLowerChar));
    }

    public static boolean getUseLowerChar()
    {
        return Boolean.parseBoolean(Settings.getProperty(SettingsItems.UseLowerChar.name()));
    }

    public static void setUseSpecialChar(boolean useSpecialChar)
    {
        Settings.setProperty(SettingsItems.UseSpecialChar.name(), String.valueOf(useSpecialChar));
    }

    public static boolean getUseSpecialChar()
    {
        return Boolean.parseBoolean(Settings.getProperty(SettingsItems.UseSpecialChar.name()));
    }

    public static void setUseNumber(boolean useNumber)
    {
        Settings.setProperty(SettingsItems.UseNumber.name(), String.valueOf(useNumber));
    }

    public static boolean getUseNumber()
    {
        return Boolean.parseBoolean(Settings.getProperty(SettingsItems.UseNumber.name()));
    }

    public static void setUseAnimatedBackground(boolean useAnimatedBackground)
    {
        Settings.setProperty(SettingsItems.UseAnimatedBackground.name(), String.valueOf(useAnimatedBackground));
    }

    public static boolean getUseAnimatedBackground()
    {
        return Boolean.parseBoolean(Settings.getProperty(SettingsItems.UseAnimatedBackground.name()));
    }

    public static void setAnimatedBackgroundOpacity(double animatedBackgroundOpacity)
    {
        Settings.setProperty(SettingsItems.AnimatedBackgroundOpacity.name(), String.valueOf(animatedBackgroundOpacity));
    }

    public static double getAnimatedBackgroundOpacity()
    {
        return Double.parseDouble(Settings.getProperty(SettingsItems.AnimatedBackgroundOpacity.name()));
    }

    public static void resetSettings(File settingsFile)
    {
        try
        {
            settingsFile.createNewFile();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        Settings = new Properties();
        Settings.setProperty(SettingsItems.LastPath.name(), "");
        Settings.setProperty(SettingsItems.StartTimes.name(), "0");
        Settings.setProperty(SettingsItems.UndoLimit.name(), "31");
        Settings.setProperty(SettingsItems.TopMost.name(), String.valueOf(false));
        Settings.setProperty(SettingsItems.UseUpperChar.name(), String.valueOf(true));
        Settings.setProperty(SettingsItems.UseLowerChar.name(), String.valueOf(true));
        Settings.setProperty(SettingsItems.UseSpecialChar.name(), String.valueOf(true));
        Settings.setProperty(SettingsItems.UseNumber.name(), String.valueOf(true));
        Settings.setProperty(SettingsItems.GenerateLength.name(), String.valueOf(10));
        Settings.setProperty(SettingsItems.UseAnimatedBackground.name(), String.valueOf(true));
        Settings.setProperty(SettingsItems.AnimatedBackgroundOpacity.name(), "1.0");
        saveSettings(settingsFile);
    }

    private static void saveSettings(File settingsFile)
    {
        try
        {
            Settings.storeToXML(new FileOutputStream(settingsFile), null, "UTF-8");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void saveSettingsFile()
    {
        saveSettings(new File(getSettingsFilePath()));
    }

    public static void loadSettingsFile(File settingsFile)
    {
        try
        {
            Settings = new Properties();
            Settings.loadFromXML(new FileInputStream(settingsFile));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
