package com.n.passwordbook.Utils;

import com.n.passwordbook.LaunchWindow.LaunchWindow;
import com.n.passwordbook.LaunchWindow.MakeSurePage;
import com.n.passwordbook.LaunchWindow.TmpDataHelper;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.util.Duration;

import java.io.IOException;

public class PageHelper
{

    private static ColorAdjust Lighten;
    private static Button DefaultButton;

    static {
        Lighten = new ColorAdjust(0,0,0.35,0);
        DefaultButton = new Button();
    }

    public static void changePage(FXMLLoader pageLoader)
    {
        try
        {
            TmpDataHelper.NowPage = pageLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        LaunchWindow.hb_desc.getChildren().add(TmpDataHelper.NowPage);
        LaunchWindow.hb_desc.getChildren().remove(0);
    }

    public static void changePageWithFadeIn(Pane page)
    {
        LaunchWindow.hb_desc.getChildren().add(page);
        LaunchWindow.hb_desc.getChildren().remove(0);
        TmpDataHelper.NowPage = page;
        enablePageFadeInAnimation(page, FadeInTime);
    }

    public static final Duration FadeInTime = Duration.seconds(0.5);

    public static <T extends Region> void enablePageFadeInAnimation(T page, Duration duration)
    {
        page.setOpacity(0);
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(duration, new KeyValue(page.opacityProperty(), 1.0)));
        timeline.play();
    }

    public static void mouseInButtonAnimation(MouseEvent e)
    {
        Button button = DefaultButton;
        button = Helper.getSpecialType(button, e.getSource());
        if (button != null)
        {
            button.setStyle("-fx-background-color: #ebeff2;");
        }
    }

    public static void mouseExitedButtonAnimation(MouseEvent e)
    {
        Button button = DefaultButton;
        button = Helper.getSpecialType(button, e.getSource());
        if (button != null)
        {
            button.setStyle("-fx-background-color: #d8e5ee;");
        }
    }

    public static void changePageToMakeSurePage(FXMLLoader pageLoader, FXMLLoader nextPageLoader, Pane lastPane)
    {
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(0.3), actionEvent -> {
            try
            {
                TmpDataHelper.NowPage = pageLoader.load();
                MakeSurePage.nextPageLoader = nextPageLoader;
                MakeSurePage.lastPage = lastPane;
                PageHelper.changePageWithFadeIn(TmpDataHelper.NowPage);
            }
            catch (IOException ioException)
            {
                ioException.printStackTrace();
            }
        }, new KeyValue(TmpDataHelper.NowPage.opacityProperty(), 0, Interpolator.SPLINE(0, 0.07, 0.59, 1.0))));
        timeline.play();
    }

    public static void changePageAndFadeOut(FXMLLoader pageLoader)
    {
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(0.3), actionEvent -> {
            try
            {
                TmpDataHelper.NowPage = pageLoader.load();
                changePageWithFadeIn(TmpDataHelper.NowPage);
            }
            catch (IOException ioException)
            {
                ioException.printStackTrace();
            }
        }, new KeyValue(TmpDataHelper.NowPage.opacityProperty(), 0, Interpolator.SPLINE(0, 0.07, 0.59, 1.0))));
        timeline.play();
    }

    public static void changePageAndFadeOut(Pane page)
    {
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(0.3), actionEvent -> {
            TmpDataHelper.NowPage = page;
            changePageWithFadeIn(TmpDataHelper.NowPage);
        }, new KeyValue(TmpDataHelper.NowPage.opacityProperty(), 0, Interpolator.SPLINE(0, 0.07, 0.59, 1.0))));
        timeline.play();
    }

    public static void imageViewOnMouseIn(MouseEvent e)
    {
        ImageView imageView = null;
        imageView = Helper.getSpecialTypeUnsafe(imageView, e.getSource());
        imageView.setEffect(Lighten);
    }

    public static void imageViewOnMouseOut(MouseEvent e)
    {
        ImageView imageView = null;
        imageView = Helper.getSpecialTypeUnsafe(imageView, e.getSource());
        imageView.setEffect(null);
    }

    public static void imageViewOnMouseClick(MouseEvent e)
    {
        ImageView imageView = null;
        imageView = Helper.getSpecialTypeUnsafe(imageView, e.getSource());
        final Timeline clickAnimationOut = new Timeline(new KeyFrame(Duration.seconds(0.3), new KeyValue(imageView.scaleXProperty(), 1), new KeyValue(imageView.scaleYProperty(), 1)));
        final Timeline clickAnimationIn = new Timeline(new KeyFrame(Duration.seconds(0.3), event -> {
            clickAnimationOut.play();
        }, new KeyValue(imageView.scaleXProperty(), 0.7), new KeyValue(imageView.scaleYProperty(), 0.7)));
        clickAnimationIn.play();
    }

    public static void lightenOnMouseIn(MouseEvent e)
    {
        try
        {
            var node = (Node)e.getSource();
            node.setStyle("-fx-background-color: #EBEFF2");
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public static void lightenOnMouseOut(MouseEvent e)
    {
        try
        {
            var node = (Node)e.getSource();
            node.setStyle("-fx-background-color: #E2E7EC");
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

}
