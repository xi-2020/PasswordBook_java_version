package com.n.passwordbook.Utils;

import java.util.Random;

public class PasswordGenerator
{
    private static final String UpperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LowerChars = "abcdefghijklmnopqrstuvwxyz";
    private static final String SpecialChars = "~!@#$%^&*(),.-_=+";

    public static String generatePassword()
    {
        boolean[] settings = {Settings.getUseUpperChar(), Settings.getUseLowerChar(), Settings.getUseSpecialChar(),Settings.getUseNumber()};
        int generateLength = Settings.getGenerateLength();
        var result = new StringBuilder(generateLength);
        var generator = new Random();

        for (int i = 0; i < generateLength; i++)
        {
            int type = generator.nextInt(4);
            while (!settings[type])
            {
                type = generator.nextInt(4);
            }
            switch (type)
            {
                case 0://生成大写字母
                {
                    result.append(UpperChars.charAt(generator.nextInt(26)));
                    break;
                }
                case 1://生成小写字母
                {
                    result.append(LowerChars.charAt(generator.nextInt(26)));
                    break;
                }
                case 2://生成特殊字符
                {
                    result.append(SpecialChars.charAt(generator.nextInt(17)));
                    break;
                }
                case 3://生成数字
                {
                    result.append(generator.nextInt(10));
                    break;
                }
            }
        }
        return result.toString();
    }
}
