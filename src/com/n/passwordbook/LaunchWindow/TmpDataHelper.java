package com.n.passwordbook.LaunchWindow;

import com.n.passwordbook.MainWindow.MainWindow;
import com.n.passwordbook.Utils.CustomFXMLClassLoader;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;

public class TmpDataHelper
{
    public static Pane NowPage;
    public static String Path;
    public static String UserName;
    public static FXMLLoader MainWindowLoader = new FXMLLoader(MainWindow.class.getResource("MainWindow.fxml"));
    public static ClassLoader CachingClassLoader = new CustomFXMLClassLoader(FXMLLoader.getDefaultClassLoader());
    public static AnchorPane MainWindowRoot;
    public static Thread MainWindowPreloadThread = new Thread(()->{
        MainWindowLoader.setClassLoader(CachingClassLoader);
        try
        {
            MainWindowRoot = MainWindowLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    });

    public static void clearAllData()
    {
        Path = null;
        UserName = null;
    }
}
