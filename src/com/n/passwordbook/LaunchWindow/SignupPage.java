package com.n.passwordbook.LaunchWindow;

import com.n.passwordbook.Utils.PageHelper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.io.File;

import static com.n.passwordbook.Utils.Helper.selectPasswordBookPath;

public class SignupPage
{
    @FXML
    public TextField tb_userName;
    @FXML
    public TextField tb_path;

    public void hlOnMousePressedBrowsePath(MouseEvent e)
    {
        if (tb_userName.getText().equals(""))
        {
            tb_userName.setText("请在此输入用户名");
            return;
        }
        File tmp = new File(tb_path.getText());
        if (!tmp.exists())
        {
            tmp = selectPasswordBookPath("选择密码本的保存文件夹");
            if (tmp == null)
            {
                return;
            }
        }
        TmpDataHelper.Path = tmp.getPath() + File.separator;
        TmpDataHelper.UserName = tb_userName.getText();
        tb_path.setText(TmpDataHelper.Path);

        PageHelper.changePageToMakeSurePage(new FXMLLoader(getClass().getResource("MakeSurePage.fxml")), new FXMLLoader(getClass().getResource("SetPasswordPage.fxml")), TmpDataHelper.NowPage);
    }
}