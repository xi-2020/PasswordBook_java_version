package com.n.passwordbook.LaunchWindow;

import com.n.passwordbook.Utils.PageHelper;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;

public class FirstLaunch
{
    public void buttonOnMouseEnteredAnimation(MouseEvent e)
    {
        PageHelper.mouseInButtonAnimation(e);
    }

    public void buttonOnMouseExitedAnimation(MouseEvent e)
    {
        PageHelper.mouseExitedButtonAnimation(e);
    }

    public void buttonOnMousePressedCreate(MouseEvent e)
    {
        PageHelper.changePageAndFadeOut(new FXMLLoader(getClass().getResource("SignupPage.fxml")));
    }

    public void buttonOnMousePressedImport(MouseEvent e)
    {
        PageHelper.changePageAndFadeOut(new FXMLLoader(getClass().getResource("ImportPage.fxml")));
    }
}
