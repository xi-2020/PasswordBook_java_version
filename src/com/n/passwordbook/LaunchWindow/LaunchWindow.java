package com.n.passwordbook.LaunchWindow;

import com.n.passwordbook.AboutWindow.AboutWindow;
import com.n.passwordbook.Utils.PageHelper;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.robot.Robot;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class LaunchWindow implements Initializable
{
    public static Pane thisPane;
    @FXML
    public Pane pane_desc;
    @FXML
    public Rectangle rec_maskLeft;
    @FXML
    public HBox hb_descRight;
    @FXML
    public static HBox hb_desc;
    @FXML
    public Rectangle rec_up;
    @FXML
    public Rectangle rec_down;
    @FXML
    public Line line_test;
    @FXML
    public Line line_up;
    @FXML
    public Line line_down;

    final Timeline tl_ShowContent = new Timeline();
    final Timeline tl_LineIn = new Timeline();


    public void windowShowAnimation()
    {
        //初始化动画前的状态
        //rec_maskLeft.setOpacity(1);
        line_test.setOpacity(0);
        line_up.setLayoutX(line_test.getLayoutX());
        line_up.setLayoutY(line_test.getLayoutY());
        line_up.setStartX(4);
        line_up.setEndX(4);
        line_down.setStartX(4);
        line_down.setEndX(4);
        line_down.setLayoutX(line_test.getLayoutX());
        line_down.setLayoutY(line_test.getLayoutY());
        pane_desc.setLayoutX(413.0);
        pane_desc.setLayoutY(161.0);
        pane_desc.setOpacity(0);
        hb_descRight.setLayoutX(351);
        hb_descRight.setLayoutY(151);
        hb_descRight.setOpacity(0);
        //初始化不变量
        final Duration duration = Duration.seconds(1.3);
        final Duration durationLineIn = Duration.seconds(1.6);
        final Duration shortDuration = Duration.seconds(1);
        final Interpolator interpolator = Interpolator.SPLINE(.27,.25,0,.87);
        final Interpolator interpolatorLineIn = Interpolator.LINEAR;
        final Interpolator interpolatorRightMove = Interpolator.SPLINE(0, .38, .3, .92);
        final Interpolator interpolatorRightFadeIn = Interpolator.SPLINE(.98, 0, .16, 1);

        //添加动画关键帧
        //直线展开动画
        tl_LineIn.getKeyFrames().add(new KeyFrame(durationLineIn, new KeyValue(line_up.startXProperty(), -70, interpolatorLineIn)));
        tl_LineIn.getKeyFrames().add(new KeyFrame(durationLineIn, new KeyValue(line_up.endXProperty(), 78, interpolatorLineIn)));
        tl_LineIn.getKeyFrames().add(new KeyFrame(durationLineIn, new KeyValue(line_down.startXProperty(), -70, interpolatorLineIn)));
        tl_LineIn.getKeyFrames().add(new KeyFrame(durationLineIn, new KeyValue(line_down.endXProperty(), 78, interpolatorLineIn)));
        tl_LineIn.getKeyFrames().add(new KeyFrame(Duration.seconds(1.2),actionAfterLineIn -> {
            //展示左侧Pane
            pane_desc.setOpacity(1);
            //左侧Pane从中心到左侧动画
            addLayoutAnimation(line_up, tl_ShowContent, 300, 156, duration, interpolator);
            addLayoutAnimation(line_down, tl_ShowContent, 300, 264, duration, interpolator);
            addLayoutAnimation(pane_desc, tl_ShowContent, 223, 159, duration, interpolator);
            addLayoutAnimation(rec_up, tl_ShowContent, 204, 153, duration, interpolator);
            addLayoutAnimation(rec_down, tl_ShowContent, 204, 268, duration, interpolator);

            tl_ShowContent.getKeyFrames().add(new KeyFrame(duration, new KeyValue(rec_up.heightProperty(), 0, interpolator)));
            tl_ShowContent.getKeyFrames().add(new KeyFrame(duration, new KeyValue(rec_down.heightProperty(), 0, interpolator)));
            tl_ShowContent.getKeyFrames().add(new KeyFrame(Duration.seconds(0.65),actionEvent -> {
                final Timeline timeline = new Timeline(
                        //右侧Pane从中心向右淡出动画
                        new KeyFrame(shortDuration, new KeyValue(hb_descRight.layoutXProperty(), 481, interpolatorRightMove)),
                        new KeyFrame(shortDuration, new KeyValue(hb_descRight.opacityProperty(), 1, interpolatorRightFadeIn))
                );
                timeline.play();
            }));
            tl_ShowContent.play();

        } ));
        tl_LineIn.play();

        new Timer().schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                Platform.runLater(() -> {
                    var robot = new Robot();
                    robot.keyPress(KeyCode.SHIFT);
                    robot.keyRelease(KeyCode.SHIFT);
                });
            }
        },1000);
    };

    public <T extends Node> void addLayoutAnimation(T node, Timeline t, double lx, double ly, Duration duration, Interpolator interpolator)
    {
        t.getKeyFrames().add(new KeyFrame(duration, new KeyValue(node.layoutXProperty(), lx, interpolator)));
        t.getKeyFrames().add(new KeyFrame(duration, new KeyValue(node.layoutYProperty(), ly, interpolator)));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        hb_descRight.getChildren().add(TmpDataHelper.NowPage);
        hb_desc = hb_descRight;
    }

    public void ivOnMouseEnteredAnimation(MouseEvent e)
    {
        PageHelper.imageViewOnMouseIn(e);
    }

    public void ivOnMouseExitedAnimation(MouseEvent e)
    {
        PageHelper.imageViewOnMouseOut(e);
    }

    public void ivOnMouseClickQuit(MouseEvent e)
    {
        System.exit(0);
    }

    public void hlAboutOnActionShowAbout(ActionEvent e){
        AboutWindow.show();
    }
}