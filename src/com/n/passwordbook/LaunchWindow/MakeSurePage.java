package com.n.passwordbook.LaunchWindow;

import com.n.passwordbook.Utils.PageHelper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

public class MakeSurePage implements Initializable
{
    @FXML
    private TextField tb_userName;

    @FXML
    private TextField tb_path;

    public static FXMLLoader nextPageLoader;
    public static Pane lastPage;
    public void buttonOnMouseEnteredAnimation(MouseEvent e)
    {
        PageHelper.mouseInButtonAnimation(e);
    }

    public void buttonOnMouseExitedAnimation(MouseEvent e)
    {
        PageHelper.mouseExitedButtonAnimation(e);
    }

    public void buttonOnMouseClickedOK(MouseEvent e)
    {

        PageHelper.changePageAndFadeOut(nextPageLoader);
    }

    public void buttonOnMouseClickedBack(MouseEvent e)
    {
        PageHelper.changePageAndFadeOut(lastPage);
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        tb_userName.setText(TmpDataHelper.UserName);
        tb_path.setText(TmpDataHelper.Path);
    }
}
