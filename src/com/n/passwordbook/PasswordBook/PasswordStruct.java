package com.n.passwordbook.PasswordBook;

import com.n.passwordbook.Utils.CryptographyHelper;
import javafx.beans.property.SimpleStringProperty;

import java.util.concurrent.atomic.AtomicInteger;

public class PasswordStruct
{
    private static AtomicInteger gobalInitializeNO = new AtomicInteger(0);

    private SimpleStringProperty _name = new SimpleStringProperty();
    private SimpleStringProperty _password = new SimpleStringProperty();
    private SimpleStringProperty _remark = new SimpleStringProperty();
    private int initializeNO = 0;
    private float sortingWeight = 0;

    public SimpleStringProperty nameProperty()
    {
        return _name;
    }

    public SimpleStringProperty passwordProperty()
    {
        return _password;
    }

    public SimpleStringProperty remarkProperty()
    {
        return _remark;
    }

    public static void clearInitializeNO()
    {
        gobalInitializeNO.set(0);
    }

    public PasswordStruct(String name, String password, String remark, int sortingWeight)
    {
        CommonSetter(name, _name);
        CommonSetter(password, _password);
        CommonSetter(remark, _remark);
        this.initializeNO = gobalInitializeNO.getAndIncrement();
        this.sortingWeight = sortingWeight;
    }

    public PasswordStruct(String name, String password, String remark, int sortingWeight, byte[] key)
    {
        CommonSetter(name, _name);//name不被加密
        CommonSetter(password, _password, key);
        CommonSetter(remark, _remark, key);
        this.initializeNO = gobalInitializeNO.getAndIncrement();
        this.sortingWeight = sortingWeight;
    }

    public String getName()
    {
        return _name.get();
    }

    public String getName(byte[] key)
    {
        return CryptographyHelper.encryptStringByAES256(_name.get(), key);
    }

    public void setName(String name)
    {
        CommonSetter(name, _name);
    }

    public void setName(String name, byte[] key)
    {
        CommonSetter(name, _name, key);
    }


    public String getPassword()
    {
        return _password.get();
    }

    public String getPassword(byte[] key)
    {
        return CommonGetter(_password, key);
    }

    public void setPassword(String password)
    {
        CommonSetter(password, _password);
    }

    public void setPassword(String password, byte[] key)
    {
        CommonSetter(password, _password, key);
    }


    public String getRemark()
    {
        return _remark.get();
    }

    public String getRemark(byte[] key)
    {
        return CommonGetter(_remark,key);
    }

    public void setRemark(String remark)
    {
        CommonSetter(remark, _remark);
    }

    public void setRemark(String remark, byte[] key)
    {
        CommonSetter(remark, _remark, key);
    }


    public int getInitializeNO()
    {
        return initializeNO;
    }


    public float getSortingWeight()
    {
        return sortingWeight;
    }

    public void setSortingWeight(float newWeight)
    {
        sortingWeight = newWeight;
    }

    private String CommonGetter(SimpleStringProperty innerStr, byte[] key)
    {
        return CryptographyHelper.decryptStringByAES256(innerStr.get(), key);
    }

    private void CommonSetter(String txt, SimpleStringProperty innerStr)
    {
        innerStr.set(txt);
    }

    private void CommonSetter(String txt, SimpleStringProperty innerStr, byte[] key)
    {
        innerStr.set(CryptographyHelper.encryptStringByAES256(txt, key));
    }
}