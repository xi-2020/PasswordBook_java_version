package com.n.passwordbook.AboutWindow;

import com.n.passwordbook.Utils.Helper;
import com.n.passwordbook.Utils.WindowHelper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class AboutWindow implements javafx.fxml.Initializable
{
    @FXML
    public ListView<String> lv_projectName;

    private static final String ResourcePath;

    private static final String AppName = "PasswordBook_java_version";

    static
    {
        ResourcePath = Objects.requireNonNull(AboutWindow.class.getResource("")).toString();
    }

    enum AboutType
    {
        LICENCE,
        NOTICE
    }

    @FXML
    public Text txt_version;
    @FXML
    public TextArea txt_showLicense;
    @FXML
    public TextArea txt_showNotice;
    @FXML
    public Text txt_title;

    public static Stage Window = null;

    public static <T extends Parent> Stage show()
    {
        if (Window != null)
        {
            Window.setAlwaysOnTop(true);
            Window.setAlwaysOnTop(false);
            return Window;
        }
        try
        {
            var url = new URL(ResourcePath + "AboutWindow.fxml");
            T root = FXMLLoader.load(url);
            var window = new Stage();
            window.setScene(new Scene(root));
            window.setTitle("关于PasswordBook_java_version");
            window.getIcons().add(Helper.Icon);
            window.setAlwaysOnTop(WindowHelper.MainStage.isAlwaysOnTop());
            window.setOnCloseRequest(e -> {
                Window = null;
            });
            window.sizeToScene();
            window.setResizable(false);
            window.show();
            window.requestFocus();
            Window = window;
            return window;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        txt_version.setText(Helper.Version);
        var fileNames = Helper.getFileNamesInJarDirectory(Helper.ProjectPath + "/AboutWindow/LICENCE");
        for (var fileName : fileNames)
        {
            lv_projectName.getItems().add(fileName);
        }
        lv_projectName.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            if (StringUtils.equals(AppName, newValue))
            {
                txt_title.setText("版权信息");
            }
            else
            {
                txt_title.setText("开源代码许可");
            }
            readAndShowTxt(newValue, AboutType.LICENCE, txt_showLicense);
            readAndShowTxt(newValue, AboutType.NOTICE, txt_showNotice);
        });
    }

    private void readAndShowTxt(String newValue, AboutType aboutType, TextArea uiComponent)
    {
        var txt = new StringBuilder();
        var appendedTxt = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(AboutWindow.class.getResourceAsStream(aboutType.name() + '/' + newValue + ".txt")))))
        {
            while ((appendedTxt = reader.readLine()) != null)
            {
                txt.append(appendedTxt);
                txt.append('\n');
            }
            uiComponent.setText(txt.toString());
            txt.setLength(0);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
